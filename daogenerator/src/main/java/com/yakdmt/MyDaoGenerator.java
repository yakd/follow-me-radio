package com.yakdmt;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;
import de.greenrobot.daogenerator.ToMany;

public class MyDaoGenerator {
    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(4, "com.yakdmt.followmeradio.database");
        Entity podcast = schema.addEntity("Podcast");
        podcast.addIdProperty();
        podcast.addStringProperty("title");
        podcast.addStringProperty("url");
        podcast.addStringProperty("banner");
        podcast.addStringProperty("icon");
        Entity podcastUrl = schema.addEntity("PodcastUrl");
        podcastUrl.addStringProperty("url").unique().primaryKey();
        Property podcastId = podcastUrl.addLongProperty("podcastId").notNull().getProperty();
        ToMany podcastToUrls = podcast.addToMany(podcastUrl, podcastId);

        new DaoGenerator().generateAll(schema, "../follow-me-radio/app/src/main/java/");
    }
}
