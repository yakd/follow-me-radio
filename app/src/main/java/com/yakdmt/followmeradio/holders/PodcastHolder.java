package com.yakdmt.followmeradio.holders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.yakdmt.followmeradio.R;
import com.yakdmt.followmeradio.database.Podcast;

/**
 * Created by yakdmt on 13/09/15.
 */
public class PodcastHolder extends RecyclerView.ViewHolder {

    private TextView mTextView;
    private ImageView mImageView;
    private Context mContext;

    public PodcastHolder(Context context, View itemView) {
        super(itemView);
        mContext = context;
        mImageView = (ImageView) itemView.findViewById(R.id.image);
        mTextView = (TextView) itemView.findViewById(R.id.title);
    }

    public void bind(Podcast podcast) {
        Picasso.with(mContext).load(podcast.getBanner()).into(mImageView);
        mTextView.setText(podcast.getTitle());
    }

}
