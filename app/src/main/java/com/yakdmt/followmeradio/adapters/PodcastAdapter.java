package com.yakdmt.followmeradio.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yakdmt.followmeradio.R;
import com.yakdmt.followmeradio.database.Podcast;
import com.yakdmt.followmeradio.holders.PodcastHolder;

import java.util.ArrayList;

/**
 * Created by yakdmt on 13/09/15.
 */
public class PodcastAdapter extends RecyclerView.Adapter{

    private ArrayList<Podcast> mFullData = new ArrayList<>();
    private ArrayList<Podcast> mData = new ArrayList<>();
    private Context mContext;

    public PodcastAdapter(Context context, ArrayList<Podcast> podcasts) {
        this.mFullData = podcasts;
        this.mData = podcasts;
        this.mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        // create a new view
        View v = LayoutInflater.from(mContext)
                .inflate(R.layout.holder_podcast, viewGroup, false);
        // set the view's size, margins, paddings and layout parameters
       PodcastHolder holder = new PodcastHolder(mContext, v);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ((PodcastHolder)viewHolder).bind(mData.get(i));
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}
