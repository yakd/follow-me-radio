package com.yakdmt.followmeradio.database;

import android.database.sqlite.SQLiteDatabase;

import com.yakdmt.followmeradio.App;

/**
 * Created by yakdmt on 13/09/15.
 */
public class DaoTask {

    private static DaoTask instance = null;

    DaoMaster.DevOpenHelper helper;
    SQLiteDatabase db;
    DaoMaster daoMaster;
    DaoSession daoSession;
    PodcastDao podcastDao;

    public DaoTask(){
        helper = new DaoMaster.DevOpenHelper(App.getContext(), "notes-db", null);
        db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        podcastDao = daoSession.getPodcastDao();
    }

    public static DaoTask getInstance(){
        if (instance == null) {
            instance = new DaoTask();
        }
        return instance;
    }

    public DaoSession getSession(){
        return daoSession;
    }
}
