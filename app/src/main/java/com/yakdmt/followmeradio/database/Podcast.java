package com.yakdmt.followmeradio.database;

import java.util.List;
import com.yakdmt.followmeradio.database.DaoSession;
import de.greenrobot.dao.DaoException;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table PODCAST.
 */
public class Podcast {

    private Long id;
    private String title;
    private String url;
    private String banner;
    private String icon;

    /** Used to resolve relations */
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    private transient PodcastDao myDao;

    private List<PodcastUrl> podcastUrlList;

    public Podcast() {
    }

    public Podcast(Long id) {
        this.id = id;
    }

    public Podcast(Long id, String title, String url, String banner, String icon) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.banner = banner;
        this.icon = icon;
    }

    /** called by internal mechanisms, do not call yourself. */
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getPodcastDao() : null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    /** To-many relationship, resolved on first access (and after reset). Changes to to-many relations are not persisted, make changes to the target entity. */
    public List<PodcastUrl> getPodcastUrlList() {
        if (podcastUrlList == null) {
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            PodcastUrlDao targetDao = daoSession.getPodcastUrlDao();
            List<PodcastUrl> podcastUrlListNew = targetDao._queryPodcast_PodcastUrlList(id);
            synchronized (this) {
                if(podcastUrlList == null) {
                    podcastUrlList = podcastUrlListNew;
                }
            }
        }
        return podcastUrlList;
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    public synchronized void resetPodcastUrlList() {
        podcastUrlList = null;
    }

    /** Convenient call for {@link AbstractDao#delete(Object)}. Entity must attached to an entity context. */
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.delete(this);
    }

    /** Convenient call for {@link AbstractDao#update(Object)}. Entity must attached to an entity context. */
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.update(this);
    }

    /** Convenient call for {@link AbstractDao#refresh(Object)}. Entity must attached to an entity context. */
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }    
        myDao.refresh(this);
    }

}
