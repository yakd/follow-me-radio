package com.yakdmt.followmeradio.utils;

import android.util.Log;

import com.google.gson.Gson;
import com.yakdmt.followmeradio.database.DaoTask;
import com.yakdmt.followmeradio.database.Podcast;
import com.yakdmt.followmeradio.database.PodcastUrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by yakdmt on 13/09/15.
 */
public class PageParser {

    private static final String JSON_DECLARATION = "Podcasts.initialJSON = ";

    public interface OnParseCompleteListener {
        void onParseComplete();

        void onParseError();
    }

    private static PageParser instance = null;

    private OnParseCompleteListener mListener;


    public static PageParser getInstance(){
        if (instance==null) {
            instance = new PageParser();
        }
        return instance;
    }
    public static PageParser withListener(OnParseCompleteListener pListener) {
        getInstance().mListener = pListener;
        return getInstance();
    }

    public void parseHtmlPage(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Document doc  = null;
                try {
                    doc = Jsoup.connect("http://radiofollow.me").get();
                    Elements elements = doc.select("script");
                    for (Element element : elements) {
                        String data = element.data();
                        if (data.contains(JSON_DECLARATION)) {
                            int position = element.data().lastIndexOf(JSON_DECLARATION);
                            position+=JSON_DECLARATION.length();
                            try {
                                JSONObject jsonObject = new JSONObject(data.substring(position));
                                JSONArray podcasts = jsonObject.getJSONArray("podcasts");
                                ArrayList<Podcast> podcastList = new ArrayList<Podcast>();
                                for (int i=0; i<podcasts.length(); i++) {
                                    JSONObject jsonPodcast = (JSONObject) podcasts.get(i);
                                    Podcast podcast = new Gson().fromJson(jsonPodcast.toString(), Podcast.class);
                                    DaoTask.getInstance().getSession().insertOrReplace(podcast);
                                    podcastList.add(parseUrls(jsonPodcast, podcast));
                                }
                                Log.i("Parser", "podcast urls count = "+podcastList.get(555).getPodcastUrlList().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    mListener.onParseComplete();
                } catch (IOException e) {
                    e.printStackTrace();
                    mListener.onParseError();
                }
            }
        }).start();
    }


    public Podcast parseUrls(JSONObject json, Podcast podcast) {
        try {
            JSONArray array = json.getJSONArray("track_urls");
            for (int i=0; i<array.length(); i++) {
                PodcastUrl podcastUrl = new PodcastUrl(array.get(i).toString(), podcast.getId());
                DaoTask.getInstance().getSession().insertOrReplace(podcastUrl);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return podcast;
    }
}
