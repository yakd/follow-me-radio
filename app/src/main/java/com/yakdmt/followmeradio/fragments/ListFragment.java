package com.yakdmt.followmeradio.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yakdmt.followmeradio.R;
import com.yakdmt.followmeradio.adapters.PodcastAdapter;
import com.yakdmt.followmeradio.database.DaoTask;
import com.yakdmt.followmeradio.database.Podcast;

import java.util.ArrayList;

/**
 * Created by yakdmt on 13/09/15.
 */
public class ListFragment extends Fragment {

    public static final String ARG_PAGE = "ARG_PAGE";

    private int mPage;
    private RecyclerView mList;

    public static ListFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        ListFragment fragment = new ListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        mList = (RecyclerView) view.findViewById(R.id.recycler_view);
        mList.setLayoutManager(new LinearLayoutManager(getActivity()));
        ArrayList<Podcast> podcasts = (ArrayList<Podcast>) DaoTask.getInstance().getSession().getPodcastDao().queryBuilder().list();
        mList.setAdapter(new PodcastAdapter(getActivity(), podcasts));
        return view;
    }
}
