package com.yakdmt.followmeradio;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.util.Log;

import com.yakdmt.followmeradio.fragments.BroadcastFragment;
import com.yakdmt.followmeradio.fragments.ListFragment;
import com.yakdmt.followmeradio.utils.PageParser;


public class MainActivity extends AppCompatActivity {

    ViewPager mViewPager;
    PagerAdapter mPagerAdapter;
    TabLayout mTabLayout;
    Toolbar mToolbar;
    AppBarLayout mAppBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setTitle(R.string.app_name);
        mAppBar = (AppBarLayout) findViewById(R.id.app_bar);
        setSupportActionBar(mToolbar);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mViewPager.setAdapter(new MainPagerAdapter(getSupportFragmentManager(),
                MainActivity.this));
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mTabLayout.setupWithViewPager(mViewPager);

        getPage();
    }

    private class MainPagerAdapter extends FragmentStatePagerAdapter {
        final int PAGE_COUNT = 4;
        private String tabTitles[] = new String[] { "Tab1", "Tab2", "Tab3", "Tab4" };
        private Context context;
        private int[] imageResId = {
                R.drawable.ic_radio_white_24dp,
                R.drawable.ic_library_music_white_24dp,
                R.drawable.ic_favorite_white_24dp,
                R.drawable.ic_file_download_white_24dp
        };

        public MainPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public Fragment getItem(int position) {
            if (position==0) {
                return BroadcastFragment.newInstance(position + 1);
            } else {
                return ListFragment.newInstance(position + 1);
            }

        }

        @Override
        public CharSequence getPageTitle(int position) {
            // Generate title based on item position
            // return tabTitles[position];
            Drawable image = context.getResources().getDrawable(imageResId[position]);
            image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
            SpannableString sb = new SpannableString(" ");
            ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
            sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return sb;
        }
    }

    public void getPage() {
        PageParser.withListener(new PageParser.OnParseCompleteListener() {
            @Override
            public void onParseComplete() {
                Log.d(MainActivity.class.getName(), "onParseComplete");
            }

            @Override
            public void onParseError() {
                Log.d(MainActivity.class.getName(), "onParseError");
            }
        }).parseHtmlPage();
    }



}
